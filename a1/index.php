<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s05-activity</title>
  </head>
  <body>
  	<?php session_start(); ?>

  
	  	<form 
	  		method="POST" 
	  		action="./server.php" 
	  		<?php echo (isset($_SESSION['user']) ? 'hidden' : ''); ?>
	  	>
	  	  <input type="hidden" name="action" value="login"><br>
	  	  Email: <input type="email" name="email" required><br>
	  	  Password: <input type="password" name="password" required><br>
	  	  <button type="submit">Login</button>
	  	</form>
	
	  	<form 
	  		method="POST" 
	  		action="./server.php" 
	  		<?php echo (isset($_SESSION['user']) ? '' : 'hidden'); ?>
	  	>
	  	  <h4>Hello! <?php echo $_SESSION['user']; ?></h4>
	  	  <input type="hidden" name="action" value="logout"><br>
	  	  <button type="submit">Logout</button>
	  	</form>
  </body>
</html>